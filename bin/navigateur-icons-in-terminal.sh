#!/usr/bin/env bash
#
# Génère une page HTML à partir de icons-in-terminal/build/icons_bash.sh
#
# La page générée est publiée à
# https://bersace.gitlab.io/powerline.bash/icons-in-terminal.html
#
set -eu

: "${ICONS_IN_TERMINAL=icons-in-terminal/}"
if ! [ -f "$ICONS_IN_TERMINAL/build/icons_bash.sh" ] ; then
	git clone --depth=1 https://github.com/sebastiencs/icons-in-terminal.git "$ICONS_IN_TERMINAL"
fi

# Charger les icônes
list_icon_vars() {
	grep -Po '^[^=]+' "$ICONS_IN_TERMINAL/build/icons_bash.sh"
}

# shellcheck source=/dev/null
. "${ICONS_IN_TERMINAL}/build/icons_bash.sh"

mapfile -t vars < <(list_icon_vars)

# Générer le HTML
cat <<EOF
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Navigateur icons-in-terminal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style type="text/css">
    @font-face {
       font-family: "icons-in-terminal";
       src: url(icons-in-terminal.ttf);
    }
    td.code {
      font-family: monospace;
      font-size: 14pt;
    }
    td.icon {
      font-family: "icons-in-terminal";
      font-size: 18pt;
    }
    </style>
  </head>
  <body class="p-5">
    <div class="container">
      <h1 class="py-3 text-center">Navigateur icons-in-terminal</h1>
      <div class="row">
    	 <p>Cette page présente toutes les icônes de la police <a href="https://github.com/sebastiencs/icons-in-terminal/">icons-in-terminal</a>.</p>
      </div>
      <div class="row mx-0 my-3 justify-content-end">
         <input type="search" placeholder="Rechercher..." class="form-control search-input w-50" data-table="icons"/>
      </div>
      <table class="table table-hover table-sm table-striped icons">
EOF

for var in "${vars[@]}" ; do
	code="${!var}"
	code="${code:2}"
	code="${code^^*}"
	# Le espaces insécables servent à présenter les ligatures (en arrière)
	# et les icônes larges (en avant).
	echo "<tr><td class=\"icon\">&nbsp; &nbsp; &nbsp; ${!var@E} &nbsp; &nbsp;</td><td class=\"code\">\u$code</td><td>$var</td></tr>"
done

cat <<EOF
      </table>
      <script>
        (function(document) {
            'use strict';

            var TableFilter = (function(myArray) {
                var search_input;

                function _onInputSearch(e) {
                    search_input = e.target;
                    var tables = document.getElementsByClassName(search_input.getAttribute('data-table'));
                    myArray.forEach.call(tables, function(table) {
                        myArray.forEach.call(table.tBodies, function(tbody) {
                            myArray.forEach.call(tbody.rows, function(row) {
				var text_content = row.textContent.toLowerCase();
				var search_val = search_input.value.toLowerCase();
				row.style.display = text_content.indexOf(search_val) > -1 ? '' : 'none';
                            });
                        });
                    });
                }

                return {
                    init: function() {
                        var inputs = document.getElementsByClassName('search-input');
                        myArray.forEach.call(inputs, function(input) {
                            input.oninput = _onInputSearch;
                        });
                    }
                };
            })(Array.prototype);

            document.addEventListener('readystatechange', function() {
                if (document.readyState === 'complete') {
                    TableFilter.init();
                }
            });

        })(document);
    </script>
  </body>
</html>
EOF
